import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import h from '../lib/tacticshelpers';
import Immutable from 'immutable';

// Import our own components
import Finished from './Finished';
import TacticsGame from './TacticsGame';
import Header from './Header';

/*
  Tactics (main app)
  This will let us make <Tactics/>
*/

const Tactics = React.createClass({
    mixins: [PureRenderMixin],

    render() {
        var data = this.props.tactics;

        // Figure out if game is finished
        var finished = '';
        if (data.get('gameFinished')) {
            finished = <Finished player1={data.get('player1')} player2={data.get('player2')} resetGame={this.props.resetGame}/>;
        }

        return (
            <div className="tactics">
                <Header tagline="Tactics"/>
                <TacticsGame player1={data.get('player1')} player2={data.get('player2')} turn={data.get('gameTurn')} changeTurn={this.props.changeTurn} hitNumber={this.props.hitNumber} addPoints={this.props.addPoints}/>
                {finished}
            </div>
        )
    }
});

export default Tactics;
