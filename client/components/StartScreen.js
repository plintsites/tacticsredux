import React from 'react';

import { Link } from 'react-router';

const StartScreen = React.createClass({
  	render() {
    	return (
			<div className="single-photo">
				<Link to="/tactics"><button type="button">Tactics</button></Link>
				<br/>
				<Link to="/leg501"><button type="button">Leg501</button></Link>
			</div>
    	)
  	}
});

export default StartScreen;
