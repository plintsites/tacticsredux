// change turns
export function changeTurn() {
  return {
    type: 'CHANGE_TURN'
  }
}

export function hitNumber(number, active) {
  return {
    type: 'HIT_NUMBER',
    number,
    active
  }
}

export function addPoints(number) {
  return {
    type: 'ADD_POINTS',
    number
  }
}

export function resetGame() {
  return {
    type: 'RESET_GAME'
  }
}
