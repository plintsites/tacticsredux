import { createStore, compose } from 'redux';
import { syncHistoryWithStore} from 'react-router-redux';
import { browserHistory } from 'react-router';
import Immutable from 'immutable';
import h from './components/games/lib/tacticshelpers';

// import the root reducer
import rootReducer from './reducers/index';

// create an object for the default data
var initData = {
    startTurn: true,
    gameTurn: true,
    gameFinished: false,
    player1: h.createPlayer('Player 1'),
    player2: h.createPlayer('Player 2'),
};

const defaultState = {
    tactics: Immutable.fromJS(initData)
};

const store = createStore(rootReducer, defaultState);

export const history = syncHistoryWithStore(browserHistory, store);

if(module.hot) {
  module.hot.accept('./reducers/',() => {
    const nextRootReducer = require('./reducers/index').default;
    store.replaceReducer(nextRootReducer);
  });
}

export default store;
